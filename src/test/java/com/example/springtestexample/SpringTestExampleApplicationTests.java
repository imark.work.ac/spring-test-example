package com.example.springtestexample;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SpringTestExampleApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void convertNumber_ReturnsOneSuccessful() {
        String result = SpringTestExampleApplication.convertNumber(1);
        assertEquals("One", result);
    }

    @Test
    void convertNumber_ReturnsDefault() {
        String result = SpringTestExampleApplication.convertNumber(55);
        assertEquals("Unknown", result);
    }

    @Test
    void convertNumber_ReturnsTwo() {
        String result = SpringTestExampleApplication.convertNumber(2);
        assertEquals("Two", result);
    }
}
