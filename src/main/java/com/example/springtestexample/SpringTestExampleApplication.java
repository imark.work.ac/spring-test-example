package com.example.springtestexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTestExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTestExampleApplication.class, args);

        System.out.println(convertNumber(1));
    }

    public static String convertNumber(int number) {

        String convertedNum;
        if (number == 1) {
            convertedNum = "One";
        } else if (number == 2) {
            convertedNum = "Two";
        } else {
            convertedNum = "Unknown";
        }

        return convertedNum;
    }
}
